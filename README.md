# Terraform



# Terraform EKS Cluster Setup using GitLab CI/CD

This repository contains Terraform scripts to provision an Amazon EKS cluster. The infrastructure setup is automated using GitLab CI/CD pipelines.

## Prerequisites

Before you begin, make sure you have the following prerequisites:

- [GitLab](https://gitlab.com/) account
- [AWS](https://aws.amazon.com/) account
- [Terraform](https://www.terraform.io/) installed locally
- AWS CLI installed and configured with necessary permissions
- GitLab Runner with Docker executor (for GitLab CI/CD)

## Getting Started

1. Clone this repository:

    ```bash
    git clone https://gitlab.com/gitlab-test7503412/Terraform.git
    cd terraform-eks-cluster
    ```

2. Create a new GitLab project and push this repository to your GitLab project:

    ```bash
    git remote set-url origin https://gitlab.com/gitlab-test7503412/Terraform.git
    git push -u origin master
    ```

3. Set up your GitLab CI/CD variables:

    - `AWS_ACCESS_KEY_ID`: AWS Access Key ID
    - `AWS_SECRET_ACCESS_KEY`: AWS Secret Access Key

4. Update Terraform backend configuration in `provider.tf`:

    

5. Run Terraform commands to initialize and apply changes:

    ```bash
    terraform init -backend-config=backend.tf
    terraform apply
    ```

## GitLab CI/CD Pipelines

The GitLab CI/CD pipelines are configured in the `.gitlab-ci.yml` file. The pipelines include stages for:

- **Terraform init**: To initilize the terraform repo
- **Terraform validate**: validate the terraofrm code
- **Terraform fmt**: Formats the terraform code
- **Terraform Plan**: Runs `terraform init` and `terraform plan` to preview changes.
- **Terraform Apply**: Applies Terraform changes to create or update the EKS cluster.

### Pipeline Configuration

Update the `.gitlab-ci.yml` file to customize the pipeline configuration based on your needs.

## Destroying the EKS Cluster

To destroy the EKS cluster and associated resources, run the following Terraform command:

```bash
terraform destroy
